[Task 4](https://developer.mozilla.org/en-US/docs/Learn/CSS/CSS_layout/Grid_skills#task_4)


Solution:
```css
.container {
display:grid;
grid-template-columns: repeat(3, 1fr);
grid-gap: 10px;

}

.tags {
display: flex;
width: auto;
flex-flow: row-reverse wrap;
justify-content: center;
align-items: center;
justify-items: center;
}
```

照片的伸縮令人感到困惑……

Ps. 對於Task 2的another question可以使用`order`屬性解決。
