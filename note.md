# note
加粗顯示爲筆者所加

# HTML

## Shadow DOM

> The shadow DOM spec has made it so that you are allowed to actually manipulate the shadow DOM of your own custom elements.

* `:host()`
* `:host-context()`

Cool! the Custom elments mean custom web components.

# CSS

## [CSS building blocks](https://developer.mozilla.org/en-US/docs/Learn/CSS/Building_blocks)

> When developing a site, **always keep overflow** in mind. Test designs with large and small amounts of content. Increase the font sizes of text. Generally ensure that your CSS works in a robust way. Changing the value of overflow to hide content, or to add scrollbars, is likely to be reserved for a few select use cases (for example, where you intend to have a scrolling box).

> Most of these units are more useful when **used for print**, rather than screen output. For example, we don't typically use cm (centimeters) on screen. The only value that you will commonly use is px (pixels).

> The key thing to remember is that each property has a defined list of **allowed value types**, and each value type has a definition explaining what the values are. 

目前沒有一個value是字符串類型的，除了:before, :after 的content屬性


## table
[Designing Tables to be Read, Not Looked At](https://alistapart.com/article/web-typography-tables/)

## Take a step back from the problem

> Any coding problem can be frustrating, especially CSS problems because you often don't get an error message to search for online to help with finding a solution. If you are becoming frustrated, take a step **away from the issue for a while** — go for a walk, grab a drink, chat to a co-worker, or work on some other thing for a while. Sometimes the solution magically appears when you stop thinking about the problem, and even if not, working on it when feeling refreshed will be much easier.

## Make a reduced test case of the problem

> If the issue isn't solved by the steps above, then you will need to do some more investigating. The best thing to do at this point is to create something known as **a reduced test case.** Being able to "reduce an issue" is a really useful skill. It will help you find problems in your own code and that of your colleagues, and will also enable you to report bugs and ask for help more effectively.

> A reduced test case is a code example that demonstrates the problem in the simplest possible way, with unrelated surrounding content and styling removed. This will often mean taking the problematic code out of your layout to make a small example which only shows that code or feature.

啊哈，十分正統的思路

# image

> Just keep in mind that replaced elements, when they become part of a grid or flex layout, **have different default behaviors**, essentially to avoid them being stretched strangely by the layout.

> > In CSS, a replaced element is an element whose representation is outside the scope of CSS; they're external objects whose representation is independent of the CSS formatting model.

因爲他們都是`Shadow DOM`

# 其他

## how brower works

[https://web.dev/howbrowserswork/](https://web.dev/howbrowserswork/)

曾经浏览过，似乎很值得细看。